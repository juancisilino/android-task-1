package com.treelineinteractive.recruitmenttask.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import com.google.android.material.R
import com.google.android.material.card.MaterialCardView
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.databinding.ViewProductItemBinding

class ProductItemView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = R.attr.materialCardViewStyle
) : MaterialCardView(context, attrs, defStyleAttr) {

    private val binding  = ViewProductItemBinding.inflate(LayoutInflater.from(context), this)
    var onClickCallback : ((productItem: ProductItem) -> Unit)? = null

    fun setProductItem(productItem: ProductItem) {
        binding.nameLabel.text = productItem.title
        binding.descriptionLabel.text = productItem.description
        binding.availableTextView.text = "Available: ${productItem.available?:0}"
        binding.removeImageView.setOnClickListener { remove(productItem) }
        binding.addImageView.setOnClickListener { add(productItem) }
    }

    private fun remove(productItem: ProductItem){
        val oldNumber = binding.countTextView.text.toString().toInt()
        when {
            productItem.available != 0 && oldNumber != 0 -> {
                val newNumber = oldNumber - 1
                val available = productItem.available - newNumber
                binding.availableTextView.text = "Available: ${available}"
                binding.countTextView.text = newNumber.toString()
                productItem.sold = newNumber
                onClickCallback?.invoke(productItem)
            }
        }
    }

    private fun add(productItem: ProductItem){
        val oldNumber = binding.countTextView.text.toString().toInt()
        when {
            productItem.available > 0 && oldNumber != productItem.available -> {
                val newNumber = oldNumber+1
                val available = productItem.available - newNumber
                binding.availableTextView.text = "Available: ${available}"
                binding.countTextView.text = newNumber.toString()
                productItem.sold = newNumber
                onClickCallback?.invoke(productItem)
            }
        }
    }
}